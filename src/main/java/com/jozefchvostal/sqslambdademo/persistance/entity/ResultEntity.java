package com.jozefchvostal.sqslambdademo.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

import java.time.Instant;
import java.util.UUID;

import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PROTECTED;


@Entity(name = "Result")
@Table(name = "RESULT")
@Data
@Builder(toBuilder = true)
@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PROTECTED, force = true)
@Accessors(fluent = true)

public class ResultEntity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @Column(columnDefinition = "RAW(16)", name = "PROCESS_ID", unique = true, nullable = false)
    private final UUID processId;

    @Column(name = "X", nullable = false)
    private final Integer x;

    @Column(name = "Y", nullable = false)
    private final Integer y;

    @Column(name = "RESULT")
    private final Integer result;

    @Column(name = "FINISHED")
    private final boolean finished;

    @Column(name = "FINISHED_DATE", updatable = false)
    private final Instant finishedDate;

}
