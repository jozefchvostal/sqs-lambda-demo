package com.jozefchvostal.sqslambdademo.listener;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.jozefchvostal.sqslambdademo.domain.Result;
import com.jozefchvostal.sqslambdademo.service.ResultService;
import io.vavr.control.Try;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class QueueListener {

    private final ResultService resultService;
    private final ObjectMapper objectMapper;

    @SqsListener(value = "https://sqs.eu-central-1.amazonaws.com/304278108811/computation-results", deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    public void onResult(String json) {
        log.info("Incoming message: {}", json);
        Result result = Try.of(() -> objectMapper.readValue(json, Result.class)).getOrElse(Result.builder().build());
        resultService.addResult(result);
    }
}