package com.jozefchvostal.sqslambdademo.mapper;

import com.jozefchvostal.sqslambdademo.domain.Result;
import com.jozefchvostal.sqslambdademo.persistance.entity.ResultEntity;
import io.vavr.Function1;
import org.springframework.stereotype.Component;

@Component
public class ResultToResultEntityMapper implements Function1<Result, ResultEntity> {

    @Override
    public ResultEntity apply(Result result) {
        return ResultEntity.builder()
                .processId(result.processId())
                .x(result.x())
                .y(result.y())
                .result(result.result())
                .finished(result.finished())
                .finishedDate(result.finishedDate())
                .build();
    }
}
