package com.jozefchvostal.sqslambdademo.mapper;

import com.jozefchvostal.sqslambdademo.domain.Result;
import com.jozefchvostal.sqslambdademo.persistance.entity.ResultEntity;
import io.vavr.Function1;
import org.springframework.stereotype.Component;

@Component
public class ResultEntityToResultMapper implements Function1<ResultEntity, Result> {

    @Override
    public Result apply(ResultEntity resultEntity) {
        return Result.builder()
                .processId(resultEntity.processId())
                .x(resultEntity.x())
                .y(resultEntity.y())
                .result(resultEntity.result())
                .finished(resultEntity.finished())
                .finishedDate(resultEntity.finishedDate())
                .build();
    }
}
