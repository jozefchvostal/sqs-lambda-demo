package com.jozefchvostal.sqslambdademo.configuration;


import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class MessagingConfig {

    @Value("${cloud.aws.credentials.access-key:}")
    private String awsAccessKeyId;

    @Value("${cloud.aws.credentials.secret-key:}")
    private String awsSecretAccessKey;

    @Primary
    @Bean("AWSCredentialsProvider")
    public AWSStaticCredentialsProvider amazonAWSCredentialsProviderDevelopment() {
        return new AWSStaticCredentialsProvider(new BasicAWSCredentials(
                awsAccessKeyId, awsSecretAccessKey));
    }
}