package com.jozefchvostal.sqslambdademo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jozefchvostal.sqslambdademo.domain.Computation;
import com.jozefchvostal.sqslambdademo.queue.AwsSQSQueueClient;
import io.vavr.control.Try;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@AllArgsConstructor
public class PublishService {

    private final AwsSQSQueueClient client;
    private final ObjectMapper objectMapper;

    public Computation requestSumComputation(Integer x, Integer y) {
        val computation = Computation.builder()
                .processId(UUID.randomUUID())
                .x(x)
                .y(y)
                .build();
        val computationMessage = Try.of(() -> objectMapper.writeValueAsString(computation))
                .getOrElse("{}");
        client.sendMessage(computationMessage);
        return computation;
    }


}
