package com.jozefchvostal.sqslambdademo.service;

import com.jozefchvostal.sqslambdademo.domain.Result;
import com.jozefchvostal.sqslambdademo.mapper.ResultEntityToResultMapper;
import com.jozefchvostal.sqslambdademo.mapper.ResultToResultEntityMapper;
import com.jozefchvostal.sqslambdademo.persistance.repository.ResultRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class ResultService {

    private final ResultRepository resultRepository;
    private final ResultEntityToResultMapper toDomainMapper;
    private final ResultToResultEntityMapper toEntityMapper;

    public Optional<Result> getResultById(UUID processId) {
        return resultRepository.findById(processId).map(toDomainMapper);
    }

    public List<Result> getAllResults() {
        return resultRepository.findAll().stream().map(toDomainMapper).collect(Collectors.toList());
    }

    public void addResult(Result result) {
        resultRepository.saveAndFlush(toEntityMapper.apply(result));
    }

}
