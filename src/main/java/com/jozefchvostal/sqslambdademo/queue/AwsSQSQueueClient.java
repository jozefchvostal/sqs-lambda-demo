package com.jozefchvostal.sqslambdademo.queue;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Component;
import com.amazonaws.services.sqs.model.SendMessageRequest;

import java.util.UUID;

@Slf4j
@Component
public class AwsSQSQueueClient {
    private final String requestsQueue = "https://sqs.eu-central-1.amazonaws.com/304278108811/computation-requests";
    private final String resultsQueue = "https://sqs.eu-central-1.amazonaws.com/304278108811/computation-results";

    private AmazonSQS client;

    private AWSStaticCredentialsProvider amazonAWSCredentialsProviderDevelopment;

    public AwsSQSQueueClient(AWSStaticCredentialsProvider amazonAWSCredentialsProviderDevelopment) {
        this.client = AmazonSQSClientBuilder.standard().withCredentials(amazonAWSCredentialsProviderDevelopment).withRegion(Regions.EU_CENTRAL_1).build();
    }


    public void sendMessage(String message) {
        val queueMessage = new SendMessageRequest(requestsQueue, message);
        val sendMessageResponse = client.sendMessage(queueMessage);
        log.info("Sent message with id {} to queue.", sendMessageResponse.getMessageId());
    }


}
