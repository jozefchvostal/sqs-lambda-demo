package com.jozefchvostal.sqslambdademo.controller;

import com.jozefchvostal.sqslambdademo.domain.Result;
import com.jozefchvostal.sqslambdademo.service.ResultService;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;

@Controller
@AllArgsConstructor
public class ResultController {

    private final ResultService resultService;

    @GetMapping("/result/{processId}")
    ResponseEntity<Result> getResult(@PathVariable("processId") UUID processId) {
        return resultService.getResultById(processId)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/results")
    ResponseEntity<List<Result>> getAllResults() {
        val results = resultService.getAllResults();
        return ResponseEntity.ok(results);
    }
}
