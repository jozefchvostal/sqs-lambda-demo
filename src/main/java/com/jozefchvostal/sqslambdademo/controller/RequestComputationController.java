package com.jozefchvostal.sqslambdademo.controller;

import com.jozefchvostal.sqslambdademo.domain.Computation;
import com.jozefchvostal.sqslambdademo.service.PublishService;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@AllArgsConstructor
public class RequestComputationController {

    private final PublishService publishService;

    @GetMapping("/sum/x/{x}/y/{y}")
    ResponseEntity<Computation> publishSumComputation(@PathVariable("x") Integer x, @PathVariable("y") Integer y) {
        val computation = publishService.requestSumComputation(x, y);
        return ResponseEntity.ok(computation);
    }


}
