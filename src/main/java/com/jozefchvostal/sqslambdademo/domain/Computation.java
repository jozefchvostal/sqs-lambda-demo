package com.jozefchvostal.sqslambdademo.domain;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.UUID;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;

@Value
@AllArgsConstructor
@Accessors(fluent = true)
@Builder(toBuilder = true)
@JsonAutoDetect(fieldVisibility = ANY)
public class Computation implements Serializable {

    private static final long serialVersionUID = 4240632192471816746L;

    private final UUID processId;

    private final Integer x;

    private final Integer y;


}
