package com.jozefchvostal.sqslambdademo.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;

@Value
@Accessors(fluent = true)
@Builder(toBuilder = true)
@JsonAutoDetect(fieldVisibility = ANY)
public class Result implements Serializable {

    private static final long serialVersionUID = 4034847740755148881L;

    private final UUID processId;

    private final Integer x;

    private final Integer y;

    private final Integer result;

    private final boolean finished;

    private final Instant finishedDate;

}
